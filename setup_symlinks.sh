#!/bin/bash -x


SCRIPT_PATH=$(dirname $(realpath -s $0))

if [ -n "$VIMRUNTIME" ]; then
    VIMDIR=$VIMRUNTIME
else
    VIMDIR="$HOME/.vim"
fi

if [ $SCRIPT_PATH = $VIMDIR ]; then
    echo "dotvim repository already at $VIMDIR"
elif [ -d "$VIMDIR" ]; then
    echo "Please remove existing $VIMDIR directory before doing this"
else
    echo "moving dotvim repository to $VIMDIR"
    mv $SCRIPT_PATH $VIMDIR
    echo "done. type 'cd .' to fix your PS1."
fi

echo "creating symbolic link ~/.vimrc"
ln -s ${VIMDIR}/dotvimrc ~/.vimrc

echo "creating symbolic link ~/.vimpagerrc"
ln -s ${VIMDIR}/dotvimpagerrc ~/.vimpagerrc

echo "creating git symlinks"
ln -s ${VIMDIR}/dotgitconfig ~/.gitconfig
ln -s ${VIMDIR}/dotgitignore ~/.gitignore

echo "creating symbolic link ~/.bashrc"
ln -s ${VIMDIR}/dotbashrc ~/.bashrc

echo "creating symbolic link ~/.bashrc"
ln -s ${VIMDIR}/dotondirrc ~/.ondirrc


echo "done."
