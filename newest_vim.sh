#!/bin/bash

git clone git@github.com:b4winckler/vim.git

sudo apt-get build-dep vim-tiny
sudo apt-get install libncurses5-dev libgnome2-dev libgnomeui-dev   libgtk2.0-dev libatk1.0-dev libbonoboui2-dev libcairo2-dev libx11-dev libxpm-dev libxt-dev

cd vim

./configure --with-features=huge --enable-pythoninterp \
            --enable-cscope --enable-multibyte \
            --without-x --disable-gui --disable-xsmp

make

sudo make install

