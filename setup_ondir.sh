#!/bin/bash

git clone git@github.com:alecthomas/ondir.git

cd ondir
make
sudo make install

echo ""
echo "WARNING: Remember to patch your setup.bash files in ros-electric using the provided patch!"
echo ""
